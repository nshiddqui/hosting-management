<?php
$this->Paginator->setTemplates([
    'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a class="page-link" href="javascript:void(0)">{{text}}</a></li>',
    'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">{{text}}</a></li>',
    'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">{{text}}</a></li>',
    'counterRange' => '{{start}} - {{end}} of {{count}}',
    'counterPages' => '{{page}} of {{pages}}',
    'first' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'last' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
]);
?>
<section class="section-header bg-primary text-white pb-7 pb-lg-11">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 text-center">
                <h1 class="display-2 mb-4">Our Blogs</h1>
                <p class="lead">We help you get better news from us, please give us feedback or report. if you got a spam blog from us. it will be glad for us</p>
            </div>
        </div>
    </div>
    <div class="pattern bottom"></div>
</section>
<section class="section section-lg line-bottom-light">
    <div class="container mt-n7 mt-lg-n12 z-2">
        <div class="row">
            <?php foreach ($blogs as $blog) { ?>
                <div class="col-lg-12 mb-5">
                    <div class="card bg-white border-light shadow-soft flex-md-row no-gutters p-4">
                        <a href="<?= $this->Url->build('/' . $blog->slug) ?>" class="col-md-6 col-lg-6">
                            <?= $this->Html->image($blog['blog_contents'][0]->image, ['class' => 'card-img-top']) ?>
                        </a>
                        <div class="card-body d-flex flex-column justify-content-between col-auto py-4 p-lg-5">
                            <a href="<?= $this->Url->build('/' . $blog->slug) ?>">
                                <h2><?= $blog['blog_contents'][0]->title ?></h2>
                            </a>
                            <p><?= $blog['blog_contents'][0]->description ?></p>
                            <div class="d-flex align-items-center">
                                <?= $this->Html->image('/favicon.ico', ['class' => 'avatar avatar-sm rounded-circle']) ?>
                                <h6 class="text-muted small ml-2 mb-0">Yuserver Blog</h6>
                                <h6 class="text-muted small font-weight-normal mb-0 ml-auto"><time datetime="2019-04-25"><?= date('F m, Y', strtotime($blog['blog_contents'][0]->created)) ?></time></h6>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="d-flex justify-content-center w-100 mt-3">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?= $this->Paginator->prev('Previous'); ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next('Next'); ?>
                </ul>
            </nav>
        </div>
    </div>
</section>