<?php
$this->assign('title', $blog['blog_contents'][0]->title);
$this->assign('description', $blog['blog_contents'][0]->description);
$this->assign('image', '/img/' . $blog['blog_contents'][0]->image);
$this->assign('keywords', $blog['blog_contents'][0]->keywords);
?>
<article>
    <!-- Hero -->
    <section class="section-header pb-7 pb-lg-10 bg-primary text-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-10 text-center">
                    <h1 class="display-3 mb-4 px-lg-5"><?= $blog['blog_contents'][0]->title ?></h1>
                    <div class="post-meta"><span class="font-weight-bold mr-3">Yuserver blog</span> <span class="post-date mr-3"><?= date('F d, Y', strtotime($blog->created)) ?></span> <span class="font-weight-bold"><?= $blog->created->timeAgoInWords(['end' => '1 year']) ?></span></div>
                </div>
            </div>
        </div>
        <div class="pattern bottom"></div>
    </section>
    <div class="section section-sm bg-white pt-5 text-black">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    <?= $this->Html->image($blog['blog_contents'][0]->image, ['class' => 'w-100']) ?>
                    <?= $this->Text->autoParagraph($blog['blog_contents'][0]->content) ?>
                </div>
            </div>
            <div class="row justify-content-sm-center align-items-center py-3 mt-3">
                <div class="col-12 col-lg-8">
                    <div class="row">
                        <div class="col-12">
                            <h6 class="font-weight-bolder d-inline mb-0 mr-3">Share:</h6>
                            <div id="shareBlock" class="d-inline-block"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
<?= $this->Html->script('c-share', ['block' => 'script']) ?>
<script>
    $('#shareBlock').cShare({
        description: `<?= $blog['blog_contents'][0]->title ?>`,
        show_buttons: [
            'fb',
            'line',
            'plurk',
            'weibo',
            'twitter',
            'tumblr',
            'pinterest',
            'email'
        ]
    });
</script>