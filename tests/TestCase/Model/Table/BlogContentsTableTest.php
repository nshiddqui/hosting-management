<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BlogContentsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BlogContentsTable Test Case
 */
class BlogContentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BlogContentsTable
     */
    protected $BlogContents;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.BlogContents',
        'app.Blogs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('BlogContents') ? [] : ['className' => BlogContentsTable::class];
        $this->BlogContents = $this->getTableLocator()->get('BlogContents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->BlogContents);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\BlogContentsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\BlogContentsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
