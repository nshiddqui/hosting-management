<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Event\EventInterface;
use Cake\Http\Client;
use Cake\Routing\Router;

/**
 * Home Controller
 *
 * @method \App\Model\Entity\Home[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeController extends AppController {

    public function beforeFilter(EventInterface $event) {
        $this->Auth->allow();
        parent::beforeFilter($event);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index() {
        $this->viewBuilder()->setLayout('public');
    }

    public function webHosting() {
        $this->loadModel('Faqs');
        $this->loadModel('Pricing');
        $faqs = $this->Faqs->getTopFiveFaq();
        $pricing = $this->Pricing->getPricing();
        $this->set(compact('faqs', 'pricing'));
        $this->viewBuilder()->setLayout('public');
    }

    public function about() {
        $this->viewBuilder()->setLayout('public');
    }

    public function analysisDomain() {
        $response = [];
        $domain = $this->request->getQuery('domain');
        if (filter_var($domain, FILTER_VALIDATE_URL)) {
            $http = new Client();
            $response = $http->post('https://lighthouse-dot-webdotdevsite.appspot.com/lh/newaudit', json_encode([
                'url' => $domain,
                'replace' => true,
                'save' => false
            ]), ['type' => 'json', 'timeout' => '40'])->getJson();
        }
        $this->set(compact('domain', 'response'));
        $this->viewBuilder()->setLayout('public');
    }

    public function privacyPolicy() {
        $this->viewBuilder()->setLayout('public');
    }

    public function refundPolicy() {
        $this->viewBuilder()->setLayout('public');
    }

    public function termsOfUse() {
        $this->viewBuilder()->setLayout('public');
    }

    public function viewBlog($slug) {
        $this->loadModel('Blogs');
        $blog = $this->Blogs->find('all', [
            'conditions' => [
                'Blogs.slug' => $slug
            ],
            'contain' => [
                'BlogContents'
            ]
        ])->first();
        $this->viewBuilder()->setLayout('public');
        $this->set(compact('blog'));
    }

    public function blogs() {
        $this->loadModel('Blogs');
        $this->paginate = [
            'contain' => ['BlogContents'],
            'order' => ['Blogs.created DESC']
        ];
        $this->set('blogs', $this->paginate($this->Blogs));
        $this->viewBuilder()->setLayout('public');
    }

    public function getLatestBlog() {
        $this->loadModel('Blogs');
        $data = $this->Blogs->find('all', [
            'conditions' => [
                'BlogContent.has_upload' => false,
                'BlogContent.keywords !=' => ''
            ],
            'contain' => ['BlogContent'],
            'order' => ['Blogs.created DESC']
        ])->first();
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function uploadSuccess($id) {
        $this->loadModel('BlogContents');
        $blog_contents = $this->BlogContents->get($id);
        $blog_contents->has_upload = 1;
        $this->BlogContents->save($blog_contents);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['success' => true]));
    }
}
